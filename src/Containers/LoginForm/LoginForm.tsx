import React, { useState, useEffect } from "react";
import { Text, View, TextInput } from "react-native";
import Button from "../../Components/Button/Button";
import CustomerOTPValidation from "../../Containers/CustomerOTPValidation/CustomerOTPValidation";
import { useForm, Controller } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import { otpRequest, setPhoneNumber, selectPhoneNumber } from '../../Features/Auth/AuthSlice';
import styles from "./LoginFormStyles";

export default function LoginForm() {
  const dispatch = useDispatch();
  const phone = useSelector(selectPhoneNumber);
  const { control, handleSubmit, formState: { errors } } = useForm();
  const [validOtpRequest, SetValidOtpRequest] = useState(false);
  const [number, onChangeNumber] = useState(false);


  const onSubmit = async (data) => {
    const { phoneNumber } = data;
    const res = await dispatch(otpRequest(phoneNumber));
    dispatch(setPhoneNumber(phoneNumber));
    if (res.payload) {
      SetValidOtpRequest(true);
    }
  };

  useEffect(() => {
    if (validOtpRequest === true) {
      console.log('phoneNumber', phone);
    }
  }, [validOtpRequest]);

  return (
    <View>
      {/* <BottomLine onChange={onPhoneChange} /> */}
      {number
        ?
        <View style={styles.container}>
          <Text style={styles.phoneNumberPlaceHolder}>הזן מספר נייד</Text>
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                maxLength={10}
                keyboardType={'numeric'}
                onChange={onChangeNumber(value)}
                style={styles.input}
              />
            )}
            name="phoneNumber"
            rules={{ required: true, minLength: 10, maxLength: 10, pattern: /[0-9]{10}/ }}
          />
          <Button title="המשך לקבלת קוד לנייד" onPress={handleSubmit(onSubmit)} />
        </View>
        :
        <View style={styles.container} >
          <Text style={styles.phoneNumberSecondary}>הזן מספר נייד</Text>
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                maxLength={10}
                keyboardType={'numeric'}
                onChange={onChangeNumber(value)}
                style={styles.input}
              />
            )}
            name="phoneNumber"
            rules={{ required: true, minLength: 10, maxLength: 10, pattern: /[0-9]{10}/ }}
          />
          <Button title="קוד ישלח לנייד שלך" style={styles.button} />
        </View>
      }
      {errors.phoneNumber && <Text style={styles.errorText}>הזן מספר תקין</Text>}

      <CustomerOTPValidation />


    </View >
  );
}

