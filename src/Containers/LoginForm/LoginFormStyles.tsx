import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
  errorText: {
    color: colors.error
  },
  phoneNumberPlaceHolder: {
    color: colors.placeHolder
  },
  phoneNumberSecondary: {
    fontWeight: 'bold',
    fontSize: 24,
    color: colors.secondary,
  },
  input: {
    marginLeft: 5,
    marginRight: 1,
    marginBottom: 18,
    borderBottomColor: colors.borderColor,
    borderBottomWidth: 1.5,
    fontSize: 20,
    textAlign: 'right',
  },
  container: {
    justifyContent: 'space-between',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
    paddingHorizontal: 32,
    borderRadius: 25,
    elevation: 3,
    backgroundColor: colors.secondary,
  },
});

