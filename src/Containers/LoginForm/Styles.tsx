import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
  errorText: {
    color: 'red',
  },
  phoneNumberPlaceHolder: {
    color: 'gray',
  },
  phoneNumberSecondary: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },
});

