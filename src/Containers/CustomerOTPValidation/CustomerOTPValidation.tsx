import React, { useState } from "react";
import { Text, View, TextInput, TouchableOpacity } from "react-native";
import Button from "../../Components/Button/Button";
import { useForm, Controller } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import Separator from '../../Components/Separator/Separator';
import { otpVerification, selectPhoneNumber } from '../../Features/Auth/AuthSlice';
import styles from "./CustomerOTPValidationStyles";

const CustomerOTPValidation = () => {
    const dispatch = useDispatch();
    const phoneNumber = useSelector(selectPhoneNumber)
    const [ValidOTP, setValidOTP] = useState();
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = async (data) => {
        const { phoneCode } = data;
        console.log('phoneCode', phoneCode);
        console.log('phone', phoneNumber);
        // console.log('phoneCode', phoneCode);
        // console.log(phoneCode);
        // console.log('phoneNumber', phoneNumber);
        // console.log('phone number');
        const res = await dispatch(otpVerification(phoneNumber, phoneCode));
        console.log('res is:', res);
    }

    return (
        <View>
            <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                        onBlur={onBlur}
                        onChangeText={value => onChange(value)}
                        value={value}
                        maxLength={4}
                        keyboardType="number-pad"
                    />
                )}
                name="phoneCode"
                rules={{ required: true, maxLength: 4, minLength: 4 }}
            />
            {errors.phoneCode && <Text style={styles.errorText}>קוד לא תקין</Text>}
            <Button title="שלח קוד" onPress={handleSubmit(onSubmit)} />

            <View style={styles.Buttom}>
                <Button title="קוד ישלח לנייד שלך" style={styles.button} />
            </View>
            <View style={styles.flexCenter}>
                <Separator />
                <Separator />
                <Separator />
                <Separator />
            </View>

            <Text>Press Here</Text>


        </View >
    )
};


export default CustomerOTPValidation;


