import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    flexCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 15,
    },
    Buttom: {
        marginTop: 298,
    },
    button: {
        // backgroundColor: 'white',
        backgroundColor: 'green',
        color: 'gray',
        paddingVertical: 16,
        paddingHorizontal: 32,
        borderRadius: 25,
    },
});

