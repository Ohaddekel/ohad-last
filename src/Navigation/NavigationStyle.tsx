import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 12,
    },
    navigationContainer: {
        backgroundColor: "white",
        marginLeft: 140,
    },
    button: {
        marginTop: 10,
    },
    navLink: {
        marginTop: 25,
        fontSize: 20,
    },
    navIcon: {
        marginLeft: 10,
    },
    image: {
        width: 30,
        height: 30,
        backgroundColor: 'black',
    }
});