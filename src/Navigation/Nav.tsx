import { StyleSheet, View, Image } from 'react-native';
import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import { Button } from 'native-base';
import { SafeAreaView } from 'react-native-safe-area-context';

const iconMenu = require('../../assets/Icons/icon-menu.png');
const iconCart = require('../../assets/Icons/icon-cart.png');


type PropsType = {
    children: React.ReactNode;
};

export const Nav: React.FC<PropsType> = ({ children }) => {
    const navigation = useNavigation();
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.navBar}>
                <Button
                    style={styles.button}
                    onPress={() => {
                        navigation.navigate('Cart');
                    }}>
                    <Image source={iconCart} style={styles.icon} />
                </Button>
                <Button
                    style={styles.button}
                    onPress={() => {
                        navigation.dispatch(DrawerActions.toggleDrawer());
                    }}>
                    <Image source={iconMenu} style={styles.icon} />
                </Button>
            </View>
            <ScrollView style={styles.content}>{children}</ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column',
    },
    navBar: {
        paddingHorizontal: 15,
        paddingBottom: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    icon: {
        width: 20,
        height: 20,
    },
    content: {
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderRadius: 20,
        flex: 1,
    },
    button: {
        height: 'auto',
        backgroundColor: 'transparent',
    },
});
