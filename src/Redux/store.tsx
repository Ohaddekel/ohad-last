import { configureStore } from '@reduxjs/toolkit';
import AuthReducer from '../Features/Auth/AuthSlice';
import CustomerReducer from '../Features/Customer/CustomerSlice';
export default configureStore({
  reducer: {
    auth: AuthReducer,
    customer: CustomerReducer,
  },
});
