import React, { useRef, useState } from "react";
import { Button, DrawerLayoutAndroid, Text, StyleSheet, View, Image, TouchableOpacity } from "react-native";
import HomeScreen from "../Screens/HomePage/HomePage";
import TopMenuNav from "../Components/TopMenuNav/TopMenuNav";

import FavIcon from "../../assets/Icons/icon-favorite";
import iconMenu from "../../assets/Icons/icon-menu.png";


import styles from "./NavigationStyle";
const App = () => {
    const drawer = useRef(null);
    const navigationView = () => (
        <View style={[styles.container, styles.navigationContainer]}>
            <Text style={styles.navLink}>
                <View>
                    <FavIcon style={styles.navIcon} onPress={() => <HomeScreen />} />
                </View>
                כניסה
            </Text >

            <Text style={styles.navLink}>
                <View>
                    <FavIcon style={styles.navIcon} onPress={() => <HomeScreen />} />
                </View>
                התחברות
            </Text >

            <Text style={styles.navLink}>
                <View onPress={() => { console.log('pressed') }}>
                    <FavIcon style={styles.navIcon} />
                </View>
                בית
            </Text >
            <Text style={styles.navLink}>
                <View>
                    <FavIcon style={styles.navIcon} onPress={() => <HomeScreen />} />
                </View>
                מוצר
            </Text >


        </View >
    );
    return (
        <DrawerLayoutAndroid
            ref={drawer}
            drawerWidth={300}
            drawerPosition={"right"}
            renderNavigationView={navigationView}
        >
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() => drawer.current.openDrawer()}>
                        <Image
                            source={require('../../assets/Icons/icon-menu.png')}
                            style={styles.image}
                        />
                    </TouchableOpacity>
                </View>
            </View>


        </DrawerLayoutAndroid>
    );
};


export default App;