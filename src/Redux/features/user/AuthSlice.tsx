import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
// import otpCustomerRequest from "../AuthThunk";
// import otpCustomerVerification from "./AuthSlice";
import { ApiService } from "../../../Services/API/API";

// import { otpRequest } from "../AuthThunk";


export const otpRequest = createAsyncThunk(
  'authentication/otpRequest',
  async (phoneNumber) => {
    try {
      const res = await ApiService.makeRequest("GET", `/customers/otp?phone=${phoneNumber}`)
      return res.data.data;
    } catch (error) {
      return (error);
    }
  }
);

export const otpVerification = createAsyncThunk(
  'authentication/otpVerification',
  async (phoneNumber, phoneCode) => {
    try {
      return ApiService.makeRequest("GET", `/customers/validate?phone={phone}&code={code}`)
    } catch (error) {
      return (error);
    }
  }
);

export const AuthSlice = createSlice({
  name: 'authentication',
  initialState: {
    phoneNumber: 'null',
    token: 'null',
  },
  reducers: {
    setPhoneNumber: (state, action) => {
      console.log('actionHere', action.payload)
      state.phoneNumber = action.payload.phoneNumber;
    },
    setToken: (state, action) => {
      state.token = action.payload;
    }
  },
  extraReducers: {
    [otpRequest.fulfilled.toString()]: (state, action) => {
      console.log('action:', action);
      // console.log('phone:', state.phoneNumber);
    },
    [otpVerification.fulfilled.toString()]: (state, action) => {
      state.token = action.payload.token;
    },
  }

  // extraReducers: {
  //   [otpRequest.pending]: (state) => {
  //     state.isFetching = true;

  //   },
  //   [otpRequest.rejected]: (state) => {
  //     state.isFetching = false;
  //     state.isError = true;
  //   },
  //   [otpRequest.fulfilled]: (state, action) => {
  //     console.log('action', action);
  //     state.isFetching = false;
  //     return state;
  //   },

  // },
});

export const selectPhoneNumber = (state) => state.authentication;
export default AuthSlice.reducer;