import { createAsyncThunk } from '@reduxjs/toolkit';
// import { ApiService } from "../../Services/API/API";

import { ApiService } from "../../Services/API/API";


export const otpRequest = createAsyncThunk(
    'authentication/otpRequest',
    async (phoneNumber) => {
        try {
            const res = await ApiService.makeRequest("GET", `/customers/otp?phone=${phoneNumber}`)
            return res.data.data;
        } catch (error) {
            return (error);
        }
    }
);




