import { createAsyncThunk } from '@reduxjs/toolkit'
import { CustomerService } from './CustomerService'

export const getCustomerMainDataThunk = createAsyncThunk(
    'customers/main-data',
    async () => {
        const response = await CustomerService.getMainData();
        return response.data.data;
    }
)