import { ApiService } from '../../Services/API/API';

export interface ICustomerService {
    getMainData: () => Promise<any>;
}

export const CustomerService: ICustomerService = {
    getMainData: () => {
        return ApiService.makeRequest("GET", `/customers/main-data`)
    }
}