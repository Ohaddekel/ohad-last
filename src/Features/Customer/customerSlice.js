import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
// import API from '../../utils/API';
import axios from 'axios';

export const loginCustomer = createAsyncThunk(
    'customers/login',
    async ({ phoneNumber }, thunkAPI) => {
        try {
            const response = await axios.get(`https://c36raxna13.execute-api.us-east-1.amazonaws.com/test/api/customers/otp?phone=${phoneNumber}`);
            if (response.status === 200) {
                let data = response.data;
                // const token = data.token;
                // localStorage.setItem('token', token);
                return data;
            } else {
                return thunkAPI.rejectWithValue(response.data);
            }
        } catch (e) {
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const customerSlice = createSlice({
    name: 'customer',
    initialState: {
        isFetching: false,
        isError: false,
    },
    extraReducers: {
        [loginCustomer.pending]: (state) => {
            state.isFetching = true;
        },
        [loginCustomer.rejected]: (state) => {
            state.isFetching = false;
            state.isError = true;
        },
        [loginCustomer.fulfilled]: (state) => {
            state.isFetching = false;
            return state;
        },
    },
});

export const selectCustomer = (state) => state.customer;
export default customerSlice.reducer;