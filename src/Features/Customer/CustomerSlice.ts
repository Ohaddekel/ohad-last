import { createSlice } from '@reduxjs/toolkit'
import { getCustomerMainDataThunk } from './CustomerThunks'

interface ICustomerState {
    mainPageData: any,
}

const initialState: ICustomerState = {
    mainPageData: null
}

export const CustomerSlice = createSlice({
    name: 'customer',
    initialState,
    reducers: {
    },
    extraReducers: {
        [getCustomerMainDataThunk.fulfilled.toString()]: (state, action) => {
            state.mainPageData = action.payload;
        }
    }
});


export default CustomerSlice.reduce