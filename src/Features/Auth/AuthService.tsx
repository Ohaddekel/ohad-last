import { ApiService } from '../../Services/API/API';

export interface IAuthService {
    otpRequest: (phoneNumber: string) => Promise<any>;
    otpVerification: (phoneNumber: string, phoneCode: string) => Promise<any>;
}
export const AuthService: IAuthService = {
    otpRequest: (phoneNumber: string) => {
        return ApiService.makeRequest("GET", `/customers/otp?phone=${phoneNumber}`)
    },
    otpVerification: (phoneNumber: string, phoneCode: string) => {
        return ApiService.makeRequest("GET", `/customers/validate?phone=${phoneNumber}&code=${phoneCode}`)
    }
}