import { createAsyncThunk } from '@reduxjs/toolkit';
import { AuthService } from "./AuthService";


export const otpRequest = createAsyncThunk(
    'authentication/otpRequest',
    async (phoneNumber) => {
        try {
            const res = await AuthService.makeRequest("GET", `/customers/otp?phone=${phoneNumber}`)
            return res.data.data;
        } catch (error) {
            return (error);
        }
    }
);




