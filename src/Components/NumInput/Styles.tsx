import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        fontWeight: 'bold',
    },
});

