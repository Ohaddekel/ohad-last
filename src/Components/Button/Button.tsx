import React from 'react';
import { Text, Pressable } from 'react-native';
import styles from "./ButtonStyles";

const Button = (props) => {
    const { onPress, title } = props;
    return (
        <Pressable style={{ ...styles.body, ...props.style }} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </Pressable>
    );
}
export default Button;