import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flexDirection: 'row',
        padding: 55,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
    },
    title: {
        marginTop: 20,
        fontSize: 20,
    },
    image: {
        backgroundColor: 'pink',
        justifyContent: 'flex-end',
    },

});

