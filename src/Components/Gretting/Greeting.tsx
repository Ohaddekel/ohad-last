import React from "react";
import { Text, View, Image } from "react-native";
import styles from "./GreetingStyles";


import GrettingImg from "../../../assets/Icons/greeting-good-afternoon";
const Gretting = () => {
    return (
        <View style={styles.container}>
            <Image
                source={require('../../../assets/image-greeting.png')}
                style={styles.image}
            />
            <View style={styles.header}>
                <GrettingImg style={styles.title} />
                <Text style={styles.title}> צברת עד היום 25 Goodies</Text>
            </View>

        </View>
    );
};
export default Gretting;