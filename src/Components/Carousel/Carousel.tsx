import React, { useState } from "react";
import { FlatList, SafeAreaView } from "react-native";
import data from "./Data";
import Item from "./CarouselCardItem";
const Carousel = () => {
    const [selectedId, setSelectedId] = useState(null);
    const renderItem = ({ item }) => {
        return (
            <Item
                item={item}
                onPress={() => setSelectedId(item.id)}
            />
        );
    };
    return (
        <SafeAreaView>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                extraData={selectedId}
                horizontal={true}
            />
        </SafeAreaView>
    );
};



export default Carousel;