import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        marginTop: 50,
        backgroundColor: 'white',
        paddingBottom: 25,
        paddingTop: 25,
        height: 210,
        width: 148,
        borderRadius: 10,
    },
    item: {
        marginLeft: 20,
        backgroundColor: 'white',
    },
    textCenter: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        resizeMode: 'contain',
        width: 130,
        height: 150,
    },
    header: {
        color: "#6CA93F",
        fontSize: 18,
        paddingLeft: 10,
        paddingTop: 12,
    },
    body: {
        color: "#222",
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20
    },
    icons: {
        margin: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});








