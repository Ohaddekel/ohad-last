import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native"

import InfoIcon from "../../../assets/Icons/icon-info";
import FavoriteIcon from "../../../assets/Icons/icon-favorite";
const FavoriteActiveIcon = require("../../../assets/Icons/icon-favorite-active");
const productImage = require('../../../assets/product-screenshot.png');
import styles from "./CarouselStyles";

const CarouselItem = ({ item, index }) => {
    const addItemToFav = () => {
        console.log('item added to fav');
    }

    const itemInfo = () => {
        console.log('info modal open');
    }
    return (
        <View style={styles.item} key={index}>
            <Image
                source={productImage}
                style={styles.image}
            />
            <View style={styles.textCenter}>
                <Text style={styles.header}>{item.title}</Text>
                <Text style={styles.body}>{item.body}</Text>
            </View>

            <View style={styles.icons}>
                <TouchableOpacity
                    onPress={itemInfo}
                >
                    <InfoIcon />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={addItemToFav}
                >
                    <FavoriteIcon />
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default CarouselItem;