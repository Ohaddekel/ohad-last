import React from 'react'
import { SafeAreaView } from 'react-native'
import CarouselCards from './CarouselCards';
export default function SnapCarousel() {
    return (
        <SafeAreaView>
            <CarouselCards />
        </SafeAreaView>
    );
}
