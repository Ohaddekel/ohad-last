import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";


export default StyleSheet.create({
    // container: {
    //     backgroundColor: 'white',
    //     borderRadius: 8,
    //     width: ITEM_WIDTH,
    //     paddingBottom: 40,
    //     shadowColor: "#000",
    //     shadowOffset: {
    //         width: 0,
    //         height: 3,
    //     },
    //     shadowOpacity: 0.29,
    //     shadowRadius: 4.65,
    //     elevation: 7,
    // },

    container: {
        backgroundColor: 'green',
    },

    image: {
        width: ITEM_WIDTH,
        height: 300,
    },
    header: {
        color: "#222",
        fontSize: 28,
        fontWeight: "bold",
        paddingLeft: 20,
        paddingTop: 20
    },
    body: {
        color: "#222",
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20
    }
});





