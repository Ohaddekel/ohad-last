import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from "react-native"
export const SLIDER_WIDTH = Dimensions.get('window').width + 80
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7)
// import Card from "../Card/Card";

import InfoIcon from "../../../assets/Icons/icon-info";
import FavoriteIcon from "../../../assets/Icons/icon-favorite";
import FavoriteActiveIcon from "../../../assets/Icons/icon-favorite-active";


const CarouselCardItem = ({ item, index }) => {
    // const onPress = () => {
    //     console.log('button pressed');
    // }
    // const addItemToFav = () => {
    //     console.log('item added to fav');
    // }

    // const itemInfo = () => {
    //     console.log('item info modal open');
    // }
    return (
        <View style={styles.container} key={index}>
            <View style={styles.center}>
                <Image
                    source={require('../../../assets/product-screenshot.png')}
                    style={styles.image}
                />
            </View>
            <View style={styles.flexContainer}>
                <Text style={styles.header}>{item.title}</Text>
                <Text style={styles.body}>{item.body}</Text>
            </View>
            <View style={styles.flexRow}>
                <TouchableOpacity
                // onPress={onPress}
                >
                    <InfoIcon />
                </TouchableOpacity>
                <TouchableOpacity
                // onPress={onPress}
                >
                    <FavoriteIcon />
                </TouchableOpacity>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        marginTop: 150,
        backgroundColor: 'white',
        paddingBottom: 25,
        paddingTop: 25,
        height: 210,
        width: 148,
        borderRadius: 10,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        resizeMode: 'contain',
        marginTop: -70,
        width: 130,
        height: 150,
    },
    header: {
        color: "#6CA93F",
        fontSize: 18,
        paddingLeft: 10,
        paddingTop: 12,
    },
    body: {
        color: "#222",
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20
    },
    flexContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexRow: {
        margin: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
})

export default CarouselCardItem