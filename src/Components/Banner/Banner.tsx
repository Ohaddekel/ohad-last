import React from 'react';
import { View, Image, Text } from 'react-native';
import styles from "./BannerStyles";
const Banner = () => {
  return (
    <Image
      source={require('../../../assets/banner.png')}
      resizeMode="contain" />
  );
};

export default Banner;
