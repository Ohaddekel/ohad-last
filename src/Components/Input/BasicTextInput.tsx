import React from 'react'
import { Controller } from 'react-hook-form';
import { View, TextInput, Text } from 'react-native'
import InputStyle from './InputStyles';
import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';



export const BasicTextInput = (props) => {
    const {
        control,
        placeholder,
        controllerName,
        defaultValue,
        errors,
        rules,
        errorMsg } = props
    const { ...rest } = props

    return (
        <>
            <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                    <Form >
                        <Item floatingLabel>
                            <Label style={InputStyle.label}>{placeholder}</Label>
                            <Input
                                style={InputStyle.input}
                                onChangeText={text => onChange(text)}
                                placeholder={placeholder}
                                value={value}
                                {...rest}
                            />
                        </Item>
                    </Form>
                )}
                name={controllerName}
                rules={rules}
                defaultValue={defaultValue}
            />
            {errors[props.controllerName] && <Text>{errorMsg}</Text>}

        </>
    )
}



