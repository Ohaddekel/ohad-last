import {StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    input: {
    height: 30,
    borderBottomColor: colors.placeHolder,
    borderBottomWidth: 1,
    marginVertical: 10,
  },
});

 