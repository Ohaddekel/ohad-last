import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import styles from "./InputStyles";

const Input = props => {
  return <TextInput {...props} style={{ ...styles.input, ...props.style }} />;
};



export default Input;
