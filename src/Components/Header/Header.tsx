import React from 'react';
import { View, Image } from 'react-native';

import styles from "./HeaderStyles";
const DisplayAnImage = () => {
    return (
        <View style={styles.body}>
            <View style={styles.container}>
                <Image style={styles.image} source={require('../../../assets/image-greeting.png')} />
            </View>
        </View>
    );
}

export default DisplayAnImage;

