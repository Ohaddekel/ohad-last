import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    container: {
        backgroundColor: 'red',
    },
    body: {
        color: colors.secondary,
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20
    }
});










