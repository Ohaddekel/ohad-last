import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";


export default StyleSheet.create({
    input: {
        marginLeft: 5,
        marginRight: 1,
        marginBottom: 18,
        marginTop: -30,
        fontSize: 10,
        borderBottomColor: colors.borderColor,
        borderBottomWidth: 1.5,
    },
});



