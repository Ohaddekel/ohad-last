import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: {
        marginLeft: 5,
        marginRight: 1,
        marginBottom: 18,
        marginTop: -30,
        fontSize: 10,
        borderBottomColor: '#ababab',
        borderBottomWidth: 1.5,
    },
});



