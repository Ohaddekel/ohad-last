import React from 'react';
import { Text } from 'react-native';
import styles from "./BodyTextStyles";

const BodyText = props => (
  <Text style={{ ...styles.body, ...props.style }}>{props.children}</Text>
);

export default BodyText;
