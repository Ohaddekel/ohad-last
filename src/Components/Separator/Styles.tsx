import {StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
   separator: {
        marginVertical: 8,
        borderBottomColor: colors.placeHolder,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});

 