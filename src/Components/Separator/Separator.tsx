import React from 'react';
import { View, TextInput } from 'react-native';
import styles from "./SeparatorStyles";

const Separator = () => (
    <TextInput maxLength={1} keyboardType={'numeric'} style={styles.separator} />
);

export default Separator;

