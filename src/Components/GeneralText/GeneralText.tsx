import React from "react";
import { Text } from "react-native";
import styles from "./GeneralTextStyles";
const GlobalText = (props) => {
    return (
        <Text
            style={styles.input}
            {...props}
        />
    );
};
export default GlobalText;