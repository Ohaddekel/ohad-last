import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    input: {
        padding: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: colors.secondary,
        fontSize: 20,

    },
});

