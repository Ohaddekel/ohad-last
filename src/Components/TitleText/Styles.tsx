import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: {
        paddingRight: 100,
        paddingTop: 80,
        fontWeight: 'bold',
        color: 'black',
    },
});

