import React from "react";
import { View, Text } from "react-native";
import styles from "./WelcomeStyles";
const Welcome = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.header}>כניסה</Text>
        </View>
    )
}

export default Welcome;