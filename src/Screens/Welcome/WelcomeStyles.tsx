import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    header: {
        fontSize: 36,
        fontWeight: 'bold',
    },
});

