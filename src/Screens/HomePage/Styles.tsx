import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    body: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: -320,
        marginLeft: 220,
    },
});

