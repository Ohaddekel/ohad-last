import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    top: {
        backgroundColor: '#F4F4F9',
    },
    bottom: {
        marginLeft: 50,
        marginTop: 15,
        padding: 20,
        flex: 1,
    },
    title: {
        marginBottom: 5,
    },
});

