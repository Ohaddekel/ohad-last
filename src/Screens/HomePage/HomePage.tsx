import React from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import Carousel from "../../Components/Carousel/Carousel";
import TitleText from "../../Components/TitleText/TitleText";
import styles from "./HomePageStyles";
import Gretting from "../../Components/Gretting/Greeting";
import Banner from "../../Components/Banner/Banner";
export default function HomeScreen() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView >
                <View style={styles.top}>
                    <Gretting />
                    <View style={styles.bottom}>
                        <TitleText style={styles.bottom}>הזמנת לאחרונה</TitleText>
                        <Carousel />
                    </View>
                    {/* <Banner /> */}
                </View>
            </ScrollView>
        </SafeAreaView>

    );
}





