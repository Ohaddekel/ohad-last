
import React from 'react';
import { View } from 'react-native';
import GeneralText from "../../Components/GeneralText/GeneralText";
import LoginForm from "../../Containers/LoginForm/LoginForm";
import styles from "./LoginScreenStyles";
function LoginScreen() {
    return (
        <View style={styles.container}>
            <View style={styles.inner}>
                <GeneralText >
                    היי rebar friend
                    בואו נבדוק אם אנחנו כבר חברים
                </GeneralText>
                <LoginForm />
            </View>
        </View>
    )
}
export default LoginScreen




