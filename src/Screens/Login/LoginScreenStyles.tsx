import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.secondary,
  },
  inner: {
    flex: 1,
    borderRadius: 28,
    borderBottomEndRadius: 40,
    padding: 15,
    marginBottom: 5,
    marginTop: 30,
    backgroundColor: colors.borderColor,
  }
});

