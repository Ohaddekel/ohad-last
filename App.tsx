import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import LoginScreen from "./src/Screens/Login/LoginScreen";
import HomeScreen from './src/Screens/HomePage/HomePage';
import store from "./src/Redux/store";
import { Provider } from 'react-redux';


const App = () => {
  return (
    <Provider store={store}>
      <View style={styles.body}>
        {/* <LoginScreen /> */}
        <HomeScreen />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});


export default App;